#+TITLE: Quicksort-exercise artifact
#+AUTHOR:VELAD
#+DATE: [2018-05-31 Thu]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

* Introduction
  This document contains the javascript artifact for
  quicksort-exercise artifact in quicksort experiment.
* Global variables
  These are the global variables that are used in this
  artifact. =width= and =height= stores the width and height of the
  layout. =root_node= stores the id and the array stored in the top
  parent node. =count= is zero for the first iteration while not zero
  after the first iteration. =speed= defines the speed of the
  transitions.
#+NAME: global-variables
#+BEGIN_SRC javascript
  var root_node,caller;
  var arr = [],size = 8;
  var count = 0;
  var width = 960, height = 500;
  var tree;
  var svg;
  var slice = [].slice;
  var speed = 300;
#+END_SRC

* api for this artifact
  =quicksortperformer= is the object that returns one object when called
  builds the basic requirements like play/pause button, user input
  textbox.
#+NAME: api
#+BEGIN_SRC javascript
  var quicksortperformer = function () {
      var obj = {};
      var resource = function(divElem){
          var dd = document.createElement("div")
          var br1 = document.createElement("br");
          var question = document.createElement("h3");
          inputgenerator();
          question.innerHTML = "perform quicksort on array " + arr.join(",");
          dd.appendChild(question)
          var btn = document.createElement("button")
          btn.innerText = "enter the answer"
          dd.appendChild(btn)
          dd.appendChild(br1)
          divElem.appendChild(dd)
          dd.id = 'showresult'
          btn.addEventListener("click", function (event) {
              funct();
              event.preventDefault();
          });
      };
      obj.resource = resource;
      return obj;

  };
#+END_SRC

* Random Input Generator
  This functions generates a random array for the user.
#+NAME: inputgen
#+BEGIN_SRC javascript
function inputgenerator()
{
    for(i=0;i<size;i++)
    {
        arr[i] = Math.floor(Math.random()*20);
    }
    return ;
}
#+END_SRC
* Draw Tree layout
  The funct() function appends a svg tree layout to the check answer
  button.
#+NAME: drawtree
#+BEGIN_SRC javascript
var funct = function () {
  if(count==0){

    var btn = document.createElement("button");
    btn.innerText = "check answer";
    btn.addEventListener("click", function (event) {
      check_answer(root_node);
      event.preventDefault();
    });
    document.getElementById("showresult").appendChild(btn);
    var d = document.createElement("div");
    document.getElementById('showresult').appendChild(d);
    d.id = "container";
    d3.select("#container").attr("style","position:relative;");
    tree = d3.layout.tree().size([920, 450]);
    svg = d3.select("#container").append("svg")
        .attr("width", width)
        .attr("height", height);
    root_node = qsort_node(arr);
    caller = setInterval(build_tree,4);
    count++;
  };
}
#+END_SRC
* Generate Id for Tree nodes
  genId() function takes a node and allot an id and array to the node.
#+NAME: genid
#+BEGIN_SRC javascript
var genId = (function () {
    var i = 0;
    return function () {
        return i++;
    };
})();
#+END_SRC
* Select Pivot
  Function choosePivot() allots the last index of the array as the
  pivot element.
#+NAME: pivotselect
#+BEGIN_SRC javascript
function choosePivot(node) {
    node.pivot = node.arr[node.arr.length-1],
        node.pivot_index = node.arr.length-1;
}
#+END_SRC
* Merge all sorted sub-arrays
  The slice() checks for the last element of the array and returns all
  elements till that. The tryMerge() function merges the sorted left
  and right sub-arrays.
#+NAME: mergearrays
#+BEGIN_SRC javascript
  function tryMerge(node) {
    var left = node.left ? undefined : [],
        right = node.right ? undefined : [];
    if (node.left && node.left.sorted) left = true  ;
    if (node.right && node.right.sorted) right = true;
    if (left && right) {
        node.sorted = true;
    } else {
        node.children.forEach(function (child) {
            step(child);
        });
    }
}
#+END_SRC
* Partition to place pivot
  The function partition() performs partition and place the pivot to
  its appropriate position according to the final soted array.
#+NAME: partitionarrays
#+BEGIN_SRC javascript
function partition(node) {
    var arr = node.arr,
        pivot = node.pivot,
        i = 0,
        j = 0,
        len = arr.length,
        temp;
    for (j=0; j < len; j++) {
        if (arr[j] <= pivot) {
                temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
                if(j==len-1)
                {
                  node.pivot_index = i;
                }
            i++;
        }
      }
      console.log(arr);
    }
#+END_SRC

* Create left and right sub-arrays by recursion
  The function createChildNodes() takes a node and performs the same
  quick sort algorithm recursively on the lest and right sub-arrys.
#+NAME: recursionleftright
#+BEGIN_SRC javascript
function createChildNodes(node) {
    var arr = node.arr, len = arr.length, i = node.pivot_index;
    if (i > 0) {
        var first_half = arr.slice(0, i);
        var left = qsort_node(first_half);
        node.children = [left];
        node.left = left;
    }
    if (i < (len - 1)) {
        var second_half = arr.slice(i + 1, len);
        var right = qsort_node(second_half);
        if (!node.children) node.children = [];
        node.children.push(right);
        node.right = right;
    }
}
#+END_SRC

* Partition the array
  The step() function checks if the array is sorted or not, by
  checking it's child nodes.
#+NAME: partitionstep
#+BEGIN_SRC javascript
function step(node) {
    var arr = node.arr,
        len = arr.length;
    if (node.children) {
        tryMerge(node);
        return;
    }
    if (len < 2) { // Base case
        node.sorted = true;
        return;
    }
    partition(node);
    createChildNodes(node);
}
#+END_SRC

* Generate a new node
  qsort_node() function initialises a node and allots an id and array
  to it.
#+NAME: nodegenerate
#+BEGIN_SRC javascript
function qsort_node(arr) {
    var node = {
        "arr": arr,
        "id": genId()
    };
    choosePivot(node);
    return node;
}
#+END_SRC

* Set node radius
  radius() function set the radius of the the node layout.
#+NAME: noderadius
#+BEGIN_SRC javascript
function radius(a) {
    return Math.sqrt(a / 2 * Math.PI);
}
#+END_SRC

* Update the Tree
  The update() function draws the tree layout after each iteration and
  defines transitions on it.
#+NAME: treeupdate
#+BEGIN_SRC javascript
var update = function (root) {
    var nodes = tree.nodes(root),
        links = tree.links(nodes);

        d3.selectAll(".sorted").style("fill","#aaa");

    var node = svg.selectAll(".node")
        .data(nodes, function (d) {
            return d.id;
        });

    node
        .transition()
        .duration(speed - 50)
        .attr("transform", function (d) {
            return "translate(" + d.x + "," + (d.y + 25) + ")";
        });

    var group = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function (d) {
            return "translate(" + d.x + "," + (d.y + 25) + ")";
        });
    node.select("circle").remove();
    node.append("circle")
        .attr("class", function (d) {
            return d.sorted ? "sorted" : "unsorted"
        })
        .attr("r", function (d) {
            return radius(d.arr.length) + 2.0;
        });
    node.select("foreignObject").remove();
    var fore = node.append("foreignObject")
                .attr("width",function(d){
                  return (d.arr.length*30) + "px";
                })
                .attr("height","50px");
        fore.select("form").remove();
        var inp = fore.append("xhtml:form")
                      .append("input")
                      .attr("type","text")
                      .attr("name","answer")
                      .style("width",function(d){
                        return (d.arr.length*30) + "px";
                      })
                      .attr("id",function(d){
                        return d.id;
                      });
    var link = svg.selectAll(".link").data(links);

    link.transition()
        .duration(speed - 50)
        .style("stroke-opacity", 1)
        .attr("d", d3.svg.diagonal());

    link.enter().append("path")
        .attr("class", "link")
        .attr("transform", function (d) {
            return "translate(0," + 25 + ")";
        })
        .attr("d", d3.svg.diagonal())
        .style("stroke-opacity", 1e-6);
    d3.selectAll(".link").style("stroke","#000").style("fill","none");
};
#+END_SRC

* Build the tree layout
#+NAME: treelayout
#+BEGIN_SRC javascript
function build_tree()
{
  step(root_node);
  update(root_node);
  if(root_node.sorted)
    clearInterval(caller);
}
#+END_SRC
* Check Answer
#+NAME: verifyanswer
#+BEGIN_SRC javascript
function check_answer(node)
{
  var ans = document.getElementById(node.id).value;
  var wrong = 0;
  ans = ans.replace(/ +/g, ' ');
  ans = ans.trim().split(" ");
  if(ans.length!=node.arr.length)
  {
    wrong = 1;
  }
  else {
    for(var i=0;i<ans.length;i++){
      ans[i]  = parseInt(ans[i]);
      if(ans[i]!=node.arr[i]){
        wrong = 1;
        break;
      }
    }
  }
  if(wrong==1){
    //fillred in input box
    document.getElementById(node.id).style.backgroundColor = "#aa0000";
  }
  else {
      //fillgreen in input box
      document.getElementById(node.id).style.backgroundColor = "#00aa00";
  }
  if(node.children){
    node.children.forEach(function(child){
      check_answer(child);
    });
  }
  return;
}
#+END_SRC
* tangling functions
#+NAME: functions
#+BEGIN_SRC javascript :tangle js/quicksort-exercise.js :eval no :noweb yes
<<global-variables>>
<<api>>
<<inputgen>>
<<drawtree>>
<<genid>>
<<pivotselect>>
<<mergearrays>>
<<partitionarrays>>
<<recursionleftright>>
<<partitionstep>>
<<nodegenerate>>
<<noderadius>>
<<treeupdate>>
<<treelayout>>
<<verifyanswer>>
#+END_SRC
